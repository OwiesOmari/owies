import com.mongodb.ErrorCategory;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;
import java.util.Scanner;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;

public class MongoDB
{
	public static void main (String [] args)
	{
         MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
          MongoDatabase database = mongoClient.getDatabase("Fdb");
         System.out.println("Successful database connection");
         MongoCollection<Document> collection = database.getCollection("col1");
         //Create Documents
         Document Student=new Document();
     	Scanner reader = new Scanner(System.in);
	     	int ch;
     	while (true)
     	{
     		System.out.println("1-Add Data");
     		System.out.println("2-Update Data");
     		System.out.println("3-Delete Data");
     		System.out.println("4-Print Data");
     		System.out.println("5- Exit");
     		
     		ch=reader.nextInt();
switch (ch)
{
case 1 :
String ID;
String Name;
String Major;
String Age;
System.out.println("please enter the student ID");
ID=reader.next();

System.out.println("please enter the student Name");
Name=reader.next();

System.out.println("please enter the student Major");
Major=reader.next();

System.out.println("please enter the student Age");
Age=reader.next();

Student.append("ID",ID)
.append("Name",Name)
.append("Major",Major)
.append("Age",Age);
collection.insertOne(Student);
	break;
	
case 2 :
	String id;
	System.out.println("please enter the ID of student you want to update his/her info");
	id=reader.next();

    // Update a document
	Document second = collection.find(Filters.eq("ID", id)).first();
    System.out.println("Original second document:");
    System.out.println(second.toJson());
    
    String newName;
    String newMajor;
    String newAge;
    
    System.out.println("please enter the new student Name");
    newName=reader.next();
    
    System.out.println("please enter the new student Major");
    newMajor=reader.next();
    
    System.out.println("please enter the new student Age");
    newAge=reader.next();
    

UpdateResult up=    collection.updateOne(new Document("ID", id),
            new Document("$set", new Document("Name",newName )
                    .append("Major", newMajor)
                    .append("Age", newAge)
                    
            )
    );

    
	break;
	
case 3:
	String DeleteID;
	System.out.println("insert the student id to delete his/her info");
	DeleteID=reader.next();
	 collection.deleteOne(Filters.gte("ID", DeleteID));
	
	break;
	
case 4 :
	List<Document> documents = (List<Document>) collection.find().into(new ArrayList<Document>());
	 
    System.out.println(documents);

	break;
	
case 5 :
	

System.exit(0);
break;
	
	

}   		
     	}
         
	}
}